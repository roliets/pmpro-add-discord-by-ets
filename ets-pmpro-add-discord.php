<?php
/**
 * Plugin Name: Paid Memberships Pro Discord- Customisation work by ExpressTech.
 * Plugin URI:  https://www.expresstechsoftwares.com
 * Description: Customisation work by ExpressTech plugin <a href="https://www.expresstechsoftwares.com">Get more plugins for your e-commerce shop on <strong>ExpressTech</strong></a>.
 * Version: 1.0.0
 * Author: ExpressTech Software Solutions Pvt. Ltd.
 * Text Domain: ets_pmpro_discord
 */

if ( ! defined( 'ABSPATH' ) ) exit;

//create plugin url constant
define('ETS_PMPRO_DISCORD_URL', plugin_dir_url(__FILE__));

//create plugin path constant
define('ETS_PMPRO_DISCORD_PATH', plugin_dir_path(__FILE__));

//discord API url
define('ETS_DISCORD_API_URL', 'https://discordapp.com/api/v6/');

define('DEFAULT_MEMVERSHIP_ID', 1);
/**
 * Class to connect discord app
 */
class Ets_Pmpro_Add_Discord
{
	function __construct()
	{
		require_once ETS_PMPRO_DISCORD_PATH.'includes/ets-pmpro-discord-admin-setting.php';
	}
}
$ets_pmpro_add_discord = new Ets_Pmpro_Add_Discord();