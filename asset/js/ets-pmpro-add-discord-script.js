jQuery(document).ready(function () {
	let discordUserName  = etsPmproParams.discord_username;
	jQuery("[id^=discord_username]").val(discordUserName);
	jQuery('#disconnect-discord').on('click',function (e) {
		e.preventDefault();
		var userId = jQuery(this).data('user-id');
		jQuery.ajax({
			type:"POST",
			dataType:"JSON",
			url:etsPmproParams.admin_ajax,
            data: {'action': 'disconnect_from_discord','user_id':userId},
			beforeSend:function () {
				jQuery('#image-loader').show();
			},
			success:function (responce) {
				if (responce.status == 1) {
					location.reload();
				}
			},
			complete:function () {
				jQuery('#image-loader').hide();
			}

		});
	});
});
